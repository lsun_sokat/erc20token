const DappToken = artifacts.require("./DappToken.sol");
const DappTokenSale = artifacts.require("./DappTokenSale.sol");
const TimeToken = artifacts.require("./TimeToken.sol");
const SoKatToken = artifacts.require("./SoKatToken.sol");
module.exports = async (deployer) => {
  await deployer.deploy(DappToken, 1000000);
  await deployer.deploy(SoKatToken, 1000000);
  const token = await DappToken.deployed();
  const tokenPrice = 1000000000000000;
  await deployer.deploy(DappTokenSale, token.address, tokenPrice);
  // await deployer.deploy(TimeToken);
};
