// SPDX-License-Identifier: MIT
pragma solidity >=0.7;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";

contract TimeToken is ERC721 {
    struct professorTime {
        uint256 start;
        uint256 end;
        string email;
        string meetinturl;
    }
    mapping(address => professorTime) _timeExists;
    address owner;

    constructor() ERC721("TimeToken", "TMET") {
        owner = msg.sender;
    }

    // return token metadata
    function getTokenMeta() public {}

    // return meeting url
    function redeem() public {}

    // add rating
    function rating() public {}

    // transfer - only allow transfer before start time

    function mint(string memory _time) public {
        // create a new token here
        // Only admin can create token or every one?
        // require()
        require(!_timeExists[msg.sender][_time]);
        times.push(_time);
        uint256 _id = times.length;
        _mint(msg.sender, _id);
        _timeExists[msg.sender][_time] = true;
    }
}
