const { assert } = require("chai");

const TimeToken = artifacts.require("./TimeToken.sol");
require("chai").use(require("chai-as-promised")).should();
contract("TimeToken", (accounts) => {
  let contract;
  before(async () => {
    contract = await TimeToken.deployed();
  });
  describe("deployment", async () => {
    it("deploys successfully", async () => {
      const address = contract.address;
      assert.notEqual(address, "");
      assert.notEqual(address, 0x0);
      assert.notEqual(address, null);
      assert.notEqual(address, undefined);
    });
    it("has a name", async () => {
      const name = await contract.name();
      assert.equal(name, "TimeToken");
    });
    it("has a symbol", async () => {
      const name = await contract.name();
      assert.equal(name, "TMET");
    });
  });
});
